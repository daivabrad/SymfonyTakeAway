<?php

namespace TakeAwayBundle\Entity;

/**
 * TypesCartePayment
 */
class TypesCartePayment
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cartesPayment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartesPayment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TypesCartePayment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add cartesPayment
     *
     * @param \TakeAwayBundle\Entity\CartePayment $cartesPayment
     *
     * @return TypesCartePayment
     */
    public function addCartesPayment(\TakeAwayBundle\Entity\CartePayment $cartesPayment)
    {
        $this->cartesPayment[] = $cartesPayment;

        return $this;
    }

    /**
     * Remove cartesPayment
     *
     * @param \TakeAwayBundle\Entity\CartePayment $cartesPayment
     */
    public function removeCartesPayment(\TakeAwayBundle\Entity\CartePayment $cartesPayment)
    {
        $this->cartesPayment->removeElement($cartesPayment);
    }

    /**
     * Get cartesPayment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartesPayment()
    {
        return $this->cartesPayment;
    }
}

