<?php

namespace TakeAwayBundle\Entity;

/**
 * CartePayment
 */
class CartePayment
{
    /**
     * @var integer
     */
    private $number;

    /**
     * @var \DateTime
     */
    private $dateExp;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \TakeAwayBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \TakeAwayBundle\Entity\TypesCartePayment
     */
    private $typescartePayment;


    /**
     * Set number
     *
     * @param integer $number
     *
     * @return CartePayment
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set dateExp
     *
     * @param \DateTime $dateExp
     *
     * @return CartePayment
     */
    public function setDateExp($dateExp)
    {
        $this->dateExp = $dateExp;

        return $this;
    }

    /**
     * Get dateExp
     *
     * @return \DateTime
     */
    public function getDateExp()
    {
        return $this->dateExp;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param \TakeAwayBundle\Entity\Customer $customer
     *
     * @return CartePayment
     */
    public function setCustomer(\TakeAwayBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \TakeAwayBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set typescartePayment
     *
     * @param \TakeAwayBundle\Entity\TypesCartePayment $typescartePayment
     *
     * @return CartePayment
     */
    public function setTypescartePayment(\TakeAwayBundle\Entity\TypesCartePayment $typescartePayment = null)
    {
        $this->typescartePayment = $typescartePayment;

        return $this;
    }

    /**
     * Get typescartePayment
     *
     * @return \TakeAwayBundle\Entity\TypesCartePayment
     */
    public function getTypescartePayment()
    {
        return $this->typescartePayment;
    }
}

