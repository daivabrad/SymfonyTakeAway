<?php

namespace TakeAwayBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
/**
 * Customer
 */
class Customer implements UserInterface
{
    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    private $passwordoriginal;
    /**
     * @var string
     */
    private $phone;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \TakeAwayBundle\Entity\Adresse
     */
    private $adresse;


    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Customer
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Customer
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Customer
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    public function getPasswordoriginal() {
        return $this->passwordoriginal;
    }

    public function setPasswordoriginal($passwordoriginal) {
        $this->passwordoriginal = $passwordoriginal;
    }

    
    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Customer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adresse
     *
     * @param \TakeAwayBundle\Entity\Adresse $adresse
     *
     * @return Customer
     */
    public function setAdresse(\TakeAwayBundle\Entity\Adresse $adresse = null)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return \TakeAwayBundle\Entity\Adresse
     */
    public function getAdresse()
    {
        return $this->adresse;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cartesPayment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $orderings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartesPayment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->orderings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cartesPayment
     *
     * @param \TakeAwayBundle\Entity\CartePayment $cartesPayment
     *
     * @return Customer
     */
    public function addCartesPayment(\TakeAwayBundle\Entity\CartePayment $cartesPayment)
    {
        $this->cartesPayment[] = $cartesPayment;

        return $this;
    }

    /**
     * Remove cartesPayment
     *
     * @param \TakeAwayBundle\Entity\CartePayment $cartesPayment
     */
    public function removeCartesPayment(\TakeAwayBundle\Entity\CartePayment $cartesPayment)
    {
        $this->cartesPayment->removeElement($cartesPayment);
    }

    /**
     * Get cartesPayment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartesPayment()
    {
        return $this->cartesPayment;
    }

    /**
     * Add ordering
     *
     * @param \TakeAwayBundle\Entity\Ordering $ordering
     *
     * @return Customer
     */
    public function addOrdering(\TakeAwayBundle\Entity\Ordering $ordering)
    {
        $this->orderings[] = $ordering;

        return $this;
    }

    /**
     * Remove ordering
     *
     * @param \TakeAwayBundle\Entity\Ordering $ordering
     */
    public function removeOrdering(\TakeAwayBundle\Entity\Ordering $ordering)
    {
        $this->orderings->removeElement($orderings);
    }

    /**
     * Get orderings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderings()
    {
        return $this->orderings;
    }
    public function eraseCredentials(){
        
    }

    public function getRoles() {
     return array('ROLES_USER');
    }
        public function getSalt() {
        return null;
    }

    public function getUsername() {
        return $this->email;
    }

}
