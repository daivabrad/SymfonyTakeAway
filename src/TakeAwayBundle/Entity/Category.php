<?php

namespace TakeAwayBundle\Entity;

/**
 * Category
 */
class Category
{
    /**
     * @var string
     */
    private $nameCat;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dishes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dishes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nameCat
     *
     * @param string $nameCat
     *
     * @return Category
     */
    public function setNameCat($nameCat)
    {
        $this->nameCat = $nameCat;

        return $this;
    }

    /**
     * Get nameCat
     *
     * @return string
     */
    public function getNameCat()
    {
        return $this->nameCat;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add dish
     *
     * @param \TakeAwayBundle\Entity\Dish $dish
     *
     * @return Category
     */
    public function addDish(\TakeAwayBundle\Entity\Dish $dish)
    {
        $this->dishes[] = $dish;

        return $this;
    }

    /**
     * Remove dish
     *
     * @param \TakeAwayBundle\Entity\Dish $dish
     */
    public function removeDish(\TakeAwayBundle\Entity\Dish $dish)
    {
        $this->dishes->removeElement($dish);
    }

    /**
     * Get dishes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDishes()
    {
        return $this->dishes;
    }
}

