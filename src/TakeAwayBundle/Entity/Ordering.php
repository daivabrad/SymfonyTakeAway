<?php

namespace TakeAwayBundle\Entity;

/**
 * Ordering
 */
class Ordering
{
    /**
     * @var \DateTime
     */
    private $dateOrder;

    /**
     * @var \DateTime
     */
    private $dateDelivery;

    /**
     * @var string
     */
    private $priceDelivery;

    /**
     * @var string
     */
    private $adresseDelivery;

    /**
     * @var string
     */
    private $totalPrice;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $items;

    /**
     * @var \TakeAwayBundle\Entity\Customer
     */
    private $customer;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set dateOrder
     *
     * @param \DateTime $dateOrder
     *
     * @return Ordering
     */
    public function setDateOrder($dateOrder)
    {
        $this->dateOrder = $dateOrder;

        return $this;
    }

    /**
     * Get dateOrder
     *
     * @return \DateTime
     */
    public function getDateOrder()
    {
        return $this->dateOrder;
    }

    /**
     * Set dateDelivery
     *
     * @param \DateTime $dateDelivery
     *
     * @return Ordering
     */
    public function setDateDelivery($dateDelivery)
    {
        $this->dateDelivery = $dateDelivery;

        return $this;
    }

    /**
     * Get dateDelivery
     *
     * @return \DateTime
     */
    public function getDateDelivery()
    {
        return $this->dateDelivery;
    }

    /**
     * Set priceDelivery
     *
     * @param string $priceDelivery
     *
     * @return Ordering
     */
    public function setPriceDelivery($priceDelivery)
    {
        $this->priceDelivery = $priceDelivery;

        return $this;
    }

    /**
     * Get priceDelivery
     *
     * @return string
     */
    public function getPriceDelivery()
    {
        return $this->priceDelivery;
    }

    /**
     * Set adresseDelivery
     *
     * @param string $adresseDelivery
     *
     * @return Ordering
     */
    public function setAdresseDelivery($adresseDelivery)
    {
        $this->adresseDelivery = $adresseDelivery;

        return $this;
    }

    /**
     * Get adresseDelivery
     *
     * @return string
     */
    public function getAdresseDelivery()
    {
        return $this->adresseDelivery;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     *
     * @return Ordering
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add item
     *
     * @param \TakeAwayBundle\Entity\Item $item
     *
     * @return Ordering
     */
    public function addItem(\TakeAwayBundle\Entity\Item $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \TakeAwayBundle\Entity\Item $item
     */
    public function removeItem(\TakeAwayBundle\Entity\Item $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set customer
     *
     * @param \TakeAwayBundle\Entity\Customer $customer
     *
     * @return Ordering
     */
    public function setCustomer(\TakeAwayBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \TakeAwayBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}

