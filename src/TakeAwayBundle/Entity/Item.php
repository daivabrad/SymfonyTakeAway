<?php

namespace TakeAwayBundle\Entity;

/**
 * Item
 */
class Item
{
    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $price;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \TakeAwayBundle\Entity\Dish
     */
    private $dish;

    /**
     * @var \TakeAwayBundle\Entity\Ordering
     */
    private $ordering;


    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Item
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dish
     *
     * @param \TakeAwayBundle\Entity\Dish $dish
     *
     * @return Item
     */
    public function setDish(\TakeAwayBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \TakeAwayBundle\Entity\Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * Set ordering
     *
     * @param \TakeAwayBundle\Entity\Ordering $ordering
     *
     * @return Item
     */
    public function setOrdering(\TakeAwayBundle\Entity\Ordering $ordering = null)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return \TakeAwayBundle\Entity\Ordering
     */
    public function getOrdering()
    {
        return $this->ordering;
    }
}

