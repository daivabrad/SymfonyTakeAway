<?php

namespace TakeAwayBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class OrderingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dateOrder', DateType::class, array(
                        'widget' => 'single_text'
                ))
                ->add('dateDelivery', DateType::class, array(
                    'widget' => 'single_text'
                ))
                ->add('adresseDelivery', TextType::class)
                ->add('priceDelivery', MoneyType::class, array(
                    'label' => 'Delivery price',
                    'data' => '5'
                ))
                ->add('totalPrice', MoneyType::class);
    }/**
     * {@inheritdoc}
     */
    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TakeAwayBundle\Entity\Ordering'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'takeawaybundle_order';
    }


}
