<?php

namespace TakeAwayBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TakeAwayBundle\Entity\TypesCartePayment;
use TakeAwayBundle\Repository\TypesCartePaymentRepository;


class CartePaymentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('number', IntegerType::class)
                ->add('dateExp', DateType::class, array(
                    'widget' => 'single_text'
                ))
                ->add('typesCartePayment', EntityType::class, [
                    'class'=> TypesCartePayment::class,
                    'query_builder'=>function(TypesCartePaymentRepository $er) {
                    return $er->createQueryBuilder('u')->orderBy('u.name', 'DESC');                    
                    },
                            'choice_label'=>function($x){
                        return strtoupper($x->getName());}
                            ])
                ->add('Pay', SubmitType::class)
                                    ->setMethod('Post')
                                    ->setAction('treatmentPayment');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TakeAwayBundle\Entity\CartePayment'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'takeawaybundle_cartepayment';
    }


}
