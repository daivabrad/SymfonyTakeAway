<?php

namespace TakeAwayBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TakeAwayBundle\Entity\Customer;
use TakeAwayBundle\Form\CustomerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TakeAwayBundle\Entity\Adresse;
use TakeAwayBundle\Entity\CartePayment;
use TakeAwayBundle\Form\CartePaymentType;
use TakeAwayBundle\Form\CategoryType;
use TakeAwayBundle\Entity\Ordering;
use TakeAwayBundle\Form\OrderingType;

class FormController extends Controller{
    
    public function accountFormAction(Request $req){
               
        $passwordEncoder=$this->get('security.password_encoder');
        $customer=new Customer();
        $adresse=new Adresse();
        $customer->setAdresse($adresse);
        
        $formCustomer=$this->createForm(CustomerType::class, $customer);
        $formCustomer->handleRequest($req);     
        
        if($formCustomer->isValid() && $formCustomer->isSubmitted()){
            $password=$passwordEncoder->encodePassword($customer, $customer->getPasswordoriginal());           
            $customer->setPassword($password); 
            $em=$this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();
//            cia mes galime eiti i Signin forma - routingo id
            return $this->redirectToRoute("signIn"); 
        }
         else{
             $vars=['accountForm'=>$formCustomer->createView()];
             return $this->render("TakeAwayViews\FormViews\AccountForm.html.twig", $vars);
         }         
    }
    

    public function signInAction(Request $req){
//        dump("hello");  sitas, kad pamatytum ar button veikia
//        die();
        $outilsAuthentification=$this->get('security.authentication_utils');
        $errors=$outilsAuthentification->getLastAuthenticationError();
        $lastEmail=$outilsAuthentification->getLastUserName();
        $vars=['errors'=>$errors,
               'lastEmail'=>$lastEmail];
        
        return $this->render("TakeAwayViews\FormViews\SignIn.html.twig", $vars); 
    }
    
    public function signOutAction(){
        throw new \Exception('Farewell!');
    }
    
    public function indexAction(){
        return $this->render("TakeAwayViews\TemplateViews\Template.html.twig"); 
    }
    
//    jei norim suzinoti vartotojo duomenis, naudojam sita: (reikia pakeisti ir security.yml)
    public function otherPageAction(){
        return $this->render("TakeAwayViews\FormViews\OtherPage.html.twig"); 
    }
    
    public function paymentFormAction(){
        $cartePayment=new CartePayment();
        $formPayment=$this->createForm("TakeAwayBundle\Form\CartePaymentType", $cartePayment,
               array('action'=>$this->generateUrl("treatmentPayment"),
                   //"treatmentPayment" yra id route, kuris veda link treatmentPaymentForm
                   // 'action'=> "/takeAway/formViews/paymentForm",
                   
                       'method'=>'POST'));
        $vars=['paymentForm'=>$formPayment->createView()];
        return $this->render("TakeAwayViews\FormViews\PaymentForm.html.twig", $vars);
    }
    
    public function treatmentPaymentFormAction(Request $req){
        $formPayment=$this->createForm(CartePaymentType::class);
        $formPayment->handleRequest($req);
        $user = $this->getUser();
         if (is_null($user)) {
            return $this->redirectToRoute("signIn");
        }
        if($formPayment->isValid() && $formPayment->isSubmitted()){
            $cartePayment=$formPayment->getData();
//            dump($cartePayment);
            $em=$this->getDoctrine()->getManager();
            $customer = $em->getRepository(Customer::class)->find($user->getId());
            $cartePayment->setCustomer($customer);
            $customer->addCartesPayment($cartePayment); 
            $em->persist($cartePayment);
            $em->flush();            
        }
                 
        $session = $this->get('session');
        $session->invalidate();
        return $this->render("TakeAwayViews\FormViews\payment_ok.html.twig");
    }
    
    public function createMenuFormAction(){
        $em=$this->getDoctrine()->getManager();
        $rep=$em->getRepository("TakeAwayBundle\Entity\Category");
        $categories=$rep->findAll();
        $vars=['allcategories'=>$categories];
        
        return $this->render("TakeAwayViews\FormViews\menuForm.html.twig", $vars);
    }
    
    public function treatmentMenuAction(Request $req){
        $formMenu=$this->createForm(CategoryType::class);
        $formMenu->handleRequest($req);
        if($formMenu->isValid && $formMenu->isSubmitted()){
            $category=$formMenu->getData();
            dump($category);
            $em-$this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
        }  
        return new Response ("Treatment is ok.");      
    }
    
    public function orderFormAction(){
        $order=new Ordering(); 
        
        $orderForm=$this->createForm("TakeAwayBundle\Form\OrderingType", $order,
               array('action'=>$this->generateUrl("treatmentOrderForm"),
                           'method'=>'POST'));
        
        $vars=['orderForm'=>$orderForm->createView()];
        return $this->render('TakeAwayViews/FormViews/order_form.html.twig', $vars);
                
    }
    
    public function treatmentOrderFormAction(Request $req){
        $orderForm=$this->createForm(OrderingType::class);
        $orderForm->handleRequest($req);   
        $user = $this->getUser();
        if (is_null($user)) {
            return $this->redirectToRoute("signIn");
        }
        
        if($orderForm->isValid() && $orderForm->isSubmitted()){
            $order=$orderForm->getData();
            $em=$this->getDoctrine()->getManager();
            $customer = $em->getRepository(Customer::class)->find($user->getId());
            $order->setCustomer($customer);
            $customer->addOrdering($order); 
            $em->persist($order);
            $em->flush();           
        }
        
        $route="paymentForm";
        return $this->redirectToRoute($route);      
    }
    

}