<?php

namespace TakeAwayBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TakeAwayBundle\Form\CustomerTypeModify;

class AdminController extends Controller{
    
    public function displayCustomersAction(){
         $em=$this->getDoctrine()->getManager(); 
         $rep=$em->getRepository("TakeAwayBundle\Entity\Customer");
         $customers=$rep->findAll();
         $vars=['customers'=>$customers];
         return $this-> render("TakeAwayViews/AdminViews/displayCustomers.html.twig", $vars); 
    }
    
    public function deleteCustomerAction($id){
        $em=$this->getDoctrine()->getManager();
        $rep=$em->getRepository("TakeAwayBundle\Entity\Customer");
        $customer=$rep->find($id);
        $em->remove($customer);
        $em->flush();
        
        $route="displayCustomers";
        return $this->redirectToRoute($route);
    }
    
       public function modifyCustomerAction(Request $req, $id){
        $em=$this->getDoctrine()->getManager();
        
        $rep=$em->getRepository("TakeAwayBundle\Entity\Customer");
        $customer=$rep->findOneBy(['id'=>$id]);
 
        $formCustomer=$this->createForm(CustomerTypeModify::class, $customer);
        $formCustomer->handleRequest($req);
        
        if($formCustomer->isValid() && $formCustomer->isSubmitted()){
            $em=$this->getDoctrine()->getManager();
            $customer=$formCustomer->getData();       
            $em->persist($customer);
            $em->flush();
            
            return $this->redirectToRoute("displayCustomers"); 
        }
       return $this->render("TakeAwayViews\FormViews\FormModifyCustomer.html.twig", array(
           'id'=>$customer->getId('id'), 
           'accountForm'=>$formCustomer->createView()));
        }
        
         public function detailsCustomerAction($id){
            $em=$this->getDoctrine()->getManager(); 
            $rep=$em->getRepository("TakeAwayBundle\Entity\Customer");
            $customer=$rep->findOneBy(['id'=>$id]);
            $vars=['onecustomer'=>$customer];
         return $this-> render("TakeAwayViews/AdminViews/details_customer.html.twig", $vars); 
    }
         
}

