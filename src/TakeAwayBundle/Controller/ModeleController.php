<?php

namespace TakeAwayBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TakeAwayBundle\Entity\Category;
use TakeAwayBundle\Entity\Dish;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;


class ModeleController extends Controller{
    
    public function insererCategoryAction(){
        $category=new Category();
        $category->setNameCat("Main dishes");
        $em=$this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();
        return new Response("Category has been inserted into the database.");
    }
    
    public function insererDishesAction(){
        $dish1=new Dish();
        $dish1->setName("Rosbif");
        $dish1->setDescription("Roast beef is a dish of beef which is roasted in an oven.");
        
        $dish2=new Dish();
        $dish2->setName("Grilled salmon");
        $dish2->setDescription("You'll need the freshest salmon fillet you can buy, "
                . "then the method is to cure (so strictly speaking it's not smoked) "
                . "over two days in a brine.");
        
//        surandam kategorija:
        $em=$this->getDoctrine()->getManager();
        $rep=$em->getRepository("TakeAwayBundle\Entity\Category");
        $category=$rep->findOneBy(array('nameCat'=>"Main dishes"));
        
        //        pridedam kategorija prie maisto:         
        $dish1->setCategory($category);   
        $dish2->setCategory($category);

//        cia pridedam maista prie kategorijos, bet mes to nematot DB
        $category->addDish($dish1);       
        $category->addDish($dish2);       

            
        $em->persist($category);
        $em->flush();
        return new Response("Dish has been inserted.");
    }
    
    public function displayDishesCategoryAction(Request $req){
        
        $session = $this->get('session');
        
        // store where we are
        $session->set('chosenCategory',$req->get('chosenCategory'));
        
        $chosenCategory= $req->get('chosenCategory');
        $em=$this->getDoctrine()->getManager(); 
        $rep=$em->getRepository("TakeAwayBundle\Entity\Category");
        $category=$rep->findOneBy(array('nameCat'=>$chosenCategory));
        $vars=['category'=>$category];
        return $this-> render("TakeAwayViews/ModeleViews/display_dishes_category.html.twig", $vars); 
    }
    
    
    public function addToBasketAction(Request $req){
        $session = $this->get('session');
        
        $basket=$session->get('basket');
        $id = $req->get("id");
        $name = $req->get("name");
        $price= $req->get("price");
        $quantity= $req->get("quantity");
        $total=0;
        
    
       if (is_null($basket)) {
           
           $basket = [];
           $basket[$id]=array(
                'name'=>$name,
                'price'=>$price,
                'quantity'=>$quantity,
                'total'=>$total
           );
        //  dump($total['price']);
       } 
       else{
            if (isset($basket[$id])){
                
                $basket[$id]=array(
                    'name'=>$name,
                    'price'=>$price,
                    'quantity'=>$basket[$id]['quantity'] + $quantity,
                    array(
                        'total'=>$basket[$id]['price'] * $basket[$id]['quantity'],
                        'totalAmount'=>$total+$total
                ));
            }
            else {
                $basket[$id]=array(
                    'name'=>$name,
                    'price'=>$price,
                    'quantity'=>$quantity,
                    'total'=>$total                
                );
            }
       }
          
       $session->set('basket', $basket);
       return new JsonResponse($basket);          
    }
        
    public function deleteBasketAction (){
        $session = $this->get('session');
        
        $session->remove("basket");
        //$session->invalidate();
        
        return $this->render("TakeAwayViews\TemplateViews\Template.html.twig"); 
    }
    
    public function deleteItemAction ($id){       
        $session = $this->get('session');
        $basket=$session->get('basket');
             
        if (isset($basket[$id])){
        
              unset($basket[$id]);
        }
        
        $session->set('basket', $basket);
        $vars=['basket'=>$basket];
        return $this->redirect('/takeAway/displayDishesCategory/'.$session->get('chosenCategory'));  
    }   
    
  
    
}


