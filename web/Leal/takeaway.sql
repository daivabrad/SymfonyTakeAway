-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2018 at 02:59 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `takeaway`
--

-- --------------------------------------------------------

--
-- Table structure for table `adresse`
--

CREATE TABLE `adresse` (
  `id` int(11) NOT NULL,
  `street` longtext COLLATE utf8mb4_unicode_ci,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` int(11) DEFAULT NULL,
  `city` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `adresse`
--

INSERT INTO `adresse` (`id`, `street`, `number`, `zipcode`, `city`) VALUES
(10, 'Marx', '98', 1263, 'Brussels'),
(11, 'Mercury', '58', 8521, 'Brussels'),
(13, 'Tesla', '854', 5214, 'Brussels'),
(14, 'Big', '45', 1230, 'Brussels'),
(16, 'Rosesandguns', '25', 1236, 'Liege'),
(18, 'Vagabond', '98', 1263, 'Brussels'),
(19, 'Lepartier', '22', 1223, 'Mons'),
(20, 'Rue Drupal', '24', 2365, 'Brussels'),
(21, 'Train', '14', 1250, 'Brussels'),
(22, 'Av. de Vernis', '12', 1320, 'Brussels');

-- --------------------------------------------------------

--
-- Table structure for table `cartepayment`
--

CREATE TABLE `cartepayment` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `date_exp` datetime DEFAULT NULL,
  `typescarte_payment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cartepayment`
--

INSERT INTO `cartepayment` (`id`, `customer_id`, `number`, `date_exp`, `typescarte_payment_id`) VALUES
(2, NULL, 12354657, '2022-01-01 00:00:00', 2),
(3, NULL, 12354657, '2022-01-01 00:00:00', 2),
(4, NULL, 23654521, '2022-01-01 00:00:00', 2),
(5, NULL, 22236554, '2023-01-01 00:00:00', 3),
(6, NULL, 254698754, '2023-01-01 00:00:00', 2),
(7, NULL, 254698754, '2023-01-01 00:00:00', 2),
(8, NULL, 551457845, '2013-01-01 00:00:00', 1),
(9, 20, 123654789, '2023-01-01 00:00:00', 3),
(10, 25, 123654521, '2023-05-04 00:00:00', 4),
(11, 27, 1456655845, '2023-01-01 00:00:00', 3),
(12, 20, 1212545558, '2020-06-24 00:00:00', 4),
(13, 20, 1212545558, '2020-06-24 00:00:00', 4),
(14, 20, 125478542, '2020-04-14 00:00:00', 1),
(15, 20, 781256456, '2020-06-25 00:00:00', 1),
(16, 20, 781256456, '2020-06-25 00:00:00', 1),
(17, 20, 781256456, '2020-06-25 00:00:00', 1),
(18, 20, 781256456, '2020-06-25 00:00:00', 1),
(19, 20, 781256456, '2020-06-25 00:00:00', 1),
(20, 20, 781256456, '2020-06-25 00:00:00', 1),
(21, 20, 123252122, '2020-04-25 00:00:00', 1),
(22, 20, 781256456, '2020-06-25 00:00:00', 1),
(23, 19, 326323555, '2020-06-24 00:00:00', 1),
(24, 19, 252541254, '2020-02-26 00:00:00', 3),
(25, 19, 254652536, '2025-04-19 00:00:00', 1),
(26, 20, 123654251, '2020-04-13 00:00:00', 4),
(27, 19, 123231564, '2020-04-24 00:00:00', 3),
(28, 19, 12365412, '2025-04-19 00:00:00', 1),
(29, 19, 12457832, '2022-04-27 00:00:00', 2),
(30, 20, 123654212, '2022-05-19 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name_cat` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name_cat`) VALUES
(1, 'Main dishes'),
(2, 'Soups'),
(3, 'Desserts'),
(4, 'Drinks'),
(5, 'Starters');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `adresse_id` int(11) DEFAULT NULL,
  `nom` longtext COLLATE utf8mb4_unicode_ci,
  `prenom` longtext COLLATE utf8mb4_unicode_ci,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `adresse_id`, `nom`, `prenom`, `email`, `password`, `phone`) VALUES
(16, 10, 'Skugr', 'Sofia', 'sofi@sofi.be', '$2y$13$4/DFjuWimTdCldq4SQqHrO57D.TnHo/wde1e6Xpz0utTCKHhducUC', '02456841'),
(17, 11, 'Braga', 'Sofia', 'sofi@sofi.com', '$2y$13$PuV.DXgMYre29xRQArzP3uMdFvjrIbDX4nVpBVFd3VCE60fPXhgiO', '012547'),
(19, 13, 'Zero', 'Sugar', 'zero@sugar.com', '$2y$13$GoMWF8e1YIS5s8/Ym4fDJOzcVCDyMsl00nvvCAK5rUk8TZA5UqCs6', '012547'),
(20, 14, 'Hepburn', 'Audrey', 'au@au.com', '$2y$13$xHTWMVCjvdTjxq8WkpNsZOA5gBvWclJFcsxKGN0r9OxMKlhdUGqGq', '0245454'),
(21, 16, 'Dupont', 'Laury', 'l@laur.be', '$2y$13$9rfNeUD/4RSh0HT6pJO1T.ThMh6sTbH.koKwt5DFbX73K9LSBvJ7G', '02456565'),
(23, 18, 'Cerna', 'Sofia', 'sofi@sofia.be', '$2y$13$uq3AS1HkY5GyxVGLjgzz0uLOO8zNCeAs40dSmpCh0G/iWKL5CMGlu', '02456841'),
(24, 19, 'Varda', 'Agnes', 'daiva@gmail.com', '$2y$13$UQmgGSIWtBs6i5fvG6PFieFSbBSUl1Z4GU/N/81RTrA3rUCmwy6sK', '02145451'),
(25, 20, 'Lavasko', 'Linda', 'linda@linda.be', '$2y$13$hfvDYtPvQuwkPhRx2wrrBenCXI7AlJjf1LtJarNq7t757QWkCv6.e', '0245872'),
(26, 21, 'Darvin', 'Josh', 'j@j.be', '$2y$13$hM7mwYyuVZ7L.2a4BXZo.eTWK1ZNfYwjLOGeNlIbi6Ot7uoecMiMe', '0125478'),
(27, 22, 'Hanks', 'Tom', 'tom@tom.com', '$2y$13$nmbvQ7A1vs/SWBNfcbuNi.NVFGrKHZ378muYD061NIAB7CmNvqTtm', '04214587');

-- --------------------------------------------------------

--
-- Table structure for table `dish`
--

CREATE TABLE `dish` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dish`
--

INSERT INTO `dish` (`id`, `category_id`, `name`, `description`, `photo`, `price`) VALUES
(1, 1, 'Cordone bleu', 'Le cordon-bleu est un plat préparé avec une escalope roulée autour de jambon et de fromage, puis panée.', 'cordone.jpg', '12.00'),
(2, 1, 'Rosbif', 'Roast beef is a dish of beef which is roasted in an oven. Roast beef is a dish of beef which is roasted in an oven.', 'rosbif.jpg', '15.00'),
(3, 1, 'Grilled salmon', 'You\'ll need the freshest salmon fillet you can buy, then the method is to cure (so strictly speaking it\'s not smoked) over two days in a brine.', 'salmon.jpg', '14.00'),
(4, 3, 'Apple cake', 'With vanilla source', 'applecake.jpg', '6.00'),
(5, 3, 'Mousse au chocolat', 'Very creamy.', 'mousse.jpg', '3.00'),
(6, 5, 'Runner bean samosas', 'Runner beans work really well with spices and these curried filo triangles are the perfect showcase - a great starter or nibble for 6 people.', 'runnerbeansamosas.jpg', '11.00'),
(7, 5, 'Chargrilled mackerel with sweet & sour beetroot', 'Grilling gives a deliciously charred quality. Pickled beetroot wedges add an extra tang in this party-perfect starter.', 'chargrilledMackerel.jpg', '8.00'),
(8, 5, 'Lobster, green bean & radicchio salad', 'This festive seafood starter, finished with a garlicky basil dressing, can be prepared ahead - ideal for a fuss-free dinner for two.', 'lobsterSalad.jpg', '12.00'),
(9, 5, 'Chicken liver & pineau pâté', 'A rich, sweet pâté recipe using pineau - a sherry-like aperitif - great as a dinner party starter.', 'chickenLiver.jpg', '17.00'),
(10, 4, 'White wine', 'France', 'whitevine.jpeg', '11.00'),
(11, 4, 'Red wine', 'France', 'redvine.jpeg', '12.00'),
(12, 4, 'Still water', 'Perrier', 'perrier.jpeg', '1.00'),
(13, 4, 'Sparkling water', 'Spa', 'spa.jpeg', '2.00'),
(15, 2, 'Vegetable & lentil soup', 'When it\'s cold outside, treat yourself to a healthy homemade vegetable soup, packed with immunity-supporting vitamin C.', 'vegetableSoup.jpg', '5.00'),
(16, 2, 'Pumpkin soup', 'The soft, sweet flesh of the Crown Prince pumpkin is perfect for this silky soup, butternut or onion squash are good alternatives.', 'pumpkinSoup.jpg', '5.00'),
(17, 2, 'Roasted sweet potato & carrot soup', 'This silky smooth, super versatile vegetarian soup is perfect for a dinner party starter or everyday dinner.', 'sweetPotatoSoup.jpg', '11.00'),
(18, 5, 'Salmon cream', 'Feed your friends this simple freeze-ahead starter, topped with goat\'s cheese croutons for added crunch.', 'starter.jpg', '7.00');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `dish_id` int(11) DEFAULT NULL,
  `ordering_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ordering`
--

CREATE TABLE `ordering` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `date_order` datetime DEFAULT NULL,
  `date_delivery` datetime DEFAULT NULL,
  `price_delivery` decimal(10,2) DEFAULT NULL,
  `adresse_delivery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ordering`
--

INSERT INTO `ordering` (`id`, `customer_id`, `date_order`, `date_delivery`, `price_delivery`, `adresse_delivery`, `total_price`) VALUES
(1, 20, '2013-01-01 00:00:00', '2013-01-01 00:00:00', NULL, 'rthtrhtrhrt', '444.00'),
(2, 20, '2018-03-26 00:00:00', '2018-03-27 00:00:00', NULL, 'Trhhgg 25, Bruxelles', '7.00'),
(3, 25, '2018-03-17 00:00:00', '2018-07-01 00:00:00', NULL, 'Rue Grand 25, Liege', '45.00'),
(4, 27, '2013-01-01 00:00:00', '2013-01-01 00:00:00', NULL, 'trakai', '25.00'),
(5, 27, '2018-04-10 00:00:00', '2018-04-10 00:00:00', NULL, 'Groblin, Brussels', '45.00'),
(6, 27, '2018-04-03 00:00:00', '2018-04-11 00:00:00', NULL, 'Roses, Bruxelles', '254.00'),
(7, 27, '2018-04-09 00:00:00', '2018-04-10 00:00:00', NULL, 'Roses, Bruxelles', '254.00'),
(8, 27, '2018-04-09 00:00:00', '2018-04-10 00:00:00', NULL, 'Roses, Bruxelles', '254.00'),
(9, 27, '2018-04-10 00:00:00', '2018-04-11 00:00:00', NULL, 'Fuco, bruxelles', '65.00'),
(10, 27, '2018-04-10 00:00:00', '2018-04-11 00:00:00', NULL, 'Fuco, bruxelles', '65.00'),
(11, 27, '2018-04-10 00:00:00', '2018-04-12 00:00:00', NULL, 'Rue Freiz 15', '58.00'),
(12, 27, '2018-04-10 00:00:00', '2018-04-13 00:00:00', NULL, 'Dragon 2', '784.00'),
(13, 19, '2018-04-10 00:00:00', '2018-04-11 00:00:00', NULL, 'ertert', '24.00'),
(14, 20, '2018-04-12 00:00:00', '2018-04-13 00:00:00', NULL, 'Trimoko 14, Brussels', '94.00'),
(15, 20, '2018-04-12 00:00:00', '2018-04-13 00:00:00', NULL, 'Trimoko 14, Brussels', '94.00'),
(16, 20, '2018-04-12 00:00:00', '2018-04-13 00:00:00', NULL, 'Tragus', '56.00'),
(17, 20, '2018-04-13 00:00:00', '2018-04-13 00:00:00', NULL, 'Rutad', '25.00'),
(18, 19, '2018-04-12 00:00:00', '2018-04-13 00:00:00', NULL, 'Trekh', '56.00'),
(19, 19, '2018-04-25 00:00:00', '2018-05-03 00:00:00', NULL, 'Max 56', '85.00'),
(20, 19, '2018-04-20 00:00:00', '2018-04-27 00:00:00', NULL, 'Franks', '25.00'),
(21, 20, '2018-04-13 00:00:00', '2018-04-13 00:00:00', '5.00', 'Rokoko', '26.00'),
(22, 19, '2018-04-13 00:00:00', '2018-04-13 00:00:00', '5.00', 'Trgfdd 15, London', '56.00'),
(23, 19, '2018-04-13 00:00:00', '2018-04-20 00:00:00', '5.00', 'Rekjh', '13.00'),
(24, 20, '2018-04-13 00:00:00', '2018-04-25 00:00:00', '5.00', 'Reashg', '17.00');

-- --------------------------------------------------------

--
-- Table structure for table `typescartepayment`
--

CREATE TABLE `typescartepayment` (
  `id` int(11) NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `typescartepayment`
--

INSERT INTO `typescartepayment` (`id`, `name`) VALUES
(1, 'Visa'),
(2, 'Maestro'),
(3, 'Bancontact'),
(4, 'Mastercard');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cartepayment`
--
ALTER TABLE `cartepayment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5573E1DB9395C3F3` (`customer_id`),
  ADD KEY `IDX_5573E1DB2661AFFC` (`typescarte_payment_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_784FEC5FE7927C74` (`email`),
  ADD KEY `IDX_784FEC5F4DE7DC5C` (`adresse_id`);

--
-- Indexes for table `dish`
--
ALTER TABLE `dish`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_354F238612469DE2` (`category_id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BF298A20148EB0CB` (`dish_id`),
  ADD KEY `IDX_BF298A208E6C7DE4` (`ordering_id`);

--
-- Indexes for table `ordering`
--
ALTER TABLE `ordering`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_824751319395C3F3` (`customer_id`);

--
-- Indexes for table `typescartepayment`
--
ALTER TABLE `typescartepayment`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `cartepayment`
--
ALTER TABLE `cartepayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `dish`
--
ALTER TABLE `dish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ordering`
--
ALTER TABLE `ordering`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `typescartepayment`
--
ALTER TABLE `typescartepayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartepayment`
--
ALTER TABLE `cartepayment`
  ADD CONSTRAINT `FK_5573E1DB2661AFFC` FOREIGN KEY (`typescarte_payment_id`) REFERENCES `typescartepayment` (`id`),
  ADD CONSTRAINT `FK_5573E1DB9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `FK_784FEC5F4DE7DC5C` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id`);

--
-- Constraints for table `dish`
--
ALTER TABLE `dish`
  ADD CONSTRAINT `FK_354F238612469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `FK_BF298A20148EB0CB` FOREIGN KEY (`dish_id`) REFERENCES `dish` (`id`),
  ADD CONSTRAINT `FK_BF298A208E6C7DE4` FOREIGN KEY (`ordering_id`) REFERENCES `ordering` (`id`);

--
-- Constraints for table `ordering`
--
ALTER TABLE `ordering`
  ADD CONSTRAINT `FK_824751319395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
