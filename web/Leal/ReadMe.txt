Pour entrer : http://takeaway.localhost/index 
On peut se loger ("Sign in") ou créer une compte ("Create an account").  
On peut remplir le panier et effacer le panier.  
On peut commander et "payer" seulement quand on est logé. 

Si on se log avec zero@sugar.com, psw : zero, on trouve Admin dans la navigation principale. 
Là-bas on peut voir la table avec des utilisateurs, on peut les effacer, modifier leurs données ou voir les détails de chacun. 
